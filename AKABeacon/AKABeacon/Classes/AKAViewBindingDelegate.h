//
//  AKAViewBindingDelegate.h
//  AKABeacon
//
//  Created by Michael Utech on 05.01.16.
//  Copyright © 2016 Michael Utech & AKA Sarl. All rights reserved.
//

@import UIKit;
@import AKACommons.AKANullability;

@class AKAViewBinding;
typedef AKAViewBinding*_Nonnull req_AKAViewBinding;


@protocol AKAViewBindingDelegate <AKABindingDelegate>

@end
