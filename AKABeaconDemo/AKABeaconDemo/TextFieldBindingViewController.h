//
//  TextFieldBindingViewController.h
//  AKABeaconDemo
//
//  Created by Michael Utech on 23.09.15.
//  Copyright © 2015 AKA Sarl. All rights reserved.
//

@import UIKit;
@import AKABeacon;

@interface TextFieldBindingViewController : AKAFormViewController

@end
